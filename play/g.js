const applist = [
  {
    name: "Snow Rider 3D",
    link: "./snowrider/",
    image: "./snowrider/TemplateData/logo.png",
    categories: ["all"],
  },
  {
    name: "Basket Bros",
    link: "./basketbros-io/",
    image: "./basketbros-io/thumb.jpg",
    categories: ["all", "2p", "mobile"],
  },
  {
    name: "Slope",
    link: "./slope/",
    image: "./slope/slope.png",
    categories: ["all"],
  },
  {
    name: "Tiny Fishing",
    link: "./tiny-fishing/",
    image: "./tiny-fishing/thumb.png",
    categories: ["all"],
  },
  {
    name: "Tomb of the Mask",
    link: "./tomb-of-the-mask/",
    image: "./tomb-of-the-mask/tomb.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Fortnite",
    link: "https://play.geforcenow.com/mall/#/deeplink?game-id=46bfab06-d864-465d-9e56-2d9e45cdee0a",
    image: "./fortnite/fortnite.png",
    categories: ["all", "2p", "beta"],
  },
  {
    name: "Paper.io 2",
    link: "./paperio2/",
    image: "./paperio2/images/icon512.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Retro Bowl",
    link: "./retrobowl/",
    image: "./retrobowl/retrobowl.jpeg",
    categories: ["all"],
  },
  {
    name:"Drift Boss",
    link: "./drift-boss/",
    image: "./drift-boss/drift-boss.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Cut the Rope", 
    link: "./cut-the-rope/",
    image: "./cut-the-rope/cut-the-rope.png",
    categories: ["all"],
  },
  {
    name: "Basketball Stars",
    link: "./basketball-stars/",
    image: "./basketball-stars/assets/images/basketball-stars.png",
    categories: ["all", "2p", "mobile"],
  },
  {
    name: "Tanuki Sunset",
    link: "./tanukisunset/",
    image: "./tanukisunset/tanukisunset.jpg",
    categories: ["all", "mobile"],
  },
  {
    name: "Drive Mad",
    link: "./drive-mad/",
    image: "./drive-mad/logo.jpg",
    categories: ["all", "mobile"],
  },
  {
    name: "Polytrack",
    link: "./polytrack/",
    image: "./polytrack/poly.png",
    categories: ["all"]
  },
  {
    name: "Getaway Shootout",
    link: "./getawayshootout/",
    image: "./getawayshootout/shootout.png",
    categories: ["all", "2p"],
  },
  {
    name: "Tennis Physics",
    link: "./tennis-physics/",
    image: "./tennis-physics/tennis.webp",
    categories: ['all', "2p"],
  },
  {
    name: "OvO",
    link: "./ovo/",
    image: "./ovo/ovo.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Stickman Hook",
    link: "./stickman-hook/",
    image: "./stickman-hook/logo.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Drift Hunters",
    link: "./drifthunters/",
    image: "./drifthunters/drift.png",
    categories: ["all"],
  },
  {
    name: "BitLife",
    link: "./bitlife/",
    image: "./bitlife/logo.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Subway Surfers",
    link: "./subway/",
    image: "./subway/4399.png",
    categories: ["all", "choice"],
  },
  {
    name: "Burrito Bison",
    link: "./burritobison/",
    image: "./burritobison/burritobison.png",
    categories: ["all", "choice", "mobile"],
  },
  {
    name: "Cookie Clicker",
    link: "./cookie/",
    image: "./cookie/cookie.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Rolly Vortex",
    link: "./rolly/",
    image: "./rolly/img.jpg",
    categories: ["all"],
  },
  {
    name: "Crazy Cars",
    link: "./crazycars/",
    image: "./crazycars/crazycars.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Rooftop Snipers",
    link: "./rooftop-snipers/",
    image: "./rooftop-snipers/rooftop-snipers-logo.jpg",
    categories: ["all", "2p"],
  },
  {
    name: "Tunnel Rush",
    link: "./tunnelrush/",
    image: "./tunnelrush/tunnelrush.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Crossy Road",
    link: "./crossy/",
    image: "./crossy/crossyroad.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Basket Random",
    link: "./basketrandom/",
    image: "./basketrandom/basketrandom.jpeg",
    categories: ["all", "2p", "mobile"],
  },
  {
    name: "Scrap Metal",
    link: "./scrapmetal/",
    image: "./scrapmetal/scrap-metal.jpg",
    categories: ["all", "choice"],
  },
  {
    name: "Temple Run",
    link: "./temple-run/",
    image: "./temple-run/img/alt.png",
    categories: ["all"],
  },
  {
    name: "Time Shooter 2",
    link: "./time-shooter-2/",
    image: "./time-shooter-2/logo.png",
    categories: ["all"],
  },
  {
    name: "Funny Shooter",
    link: "./funny/",
    image: "./funny/funny.jpeg",
    categories: ["all"],
  },
  {
    name: "Hextris",
    link: "./hextris/",
    image: "./hextris/favicon.ico",
    categories: ["all", "choice", "mobile"],
  },
  {
    name: "Moto X3M",
    link: "./motox3m/",
    image: "./motox3m/motox3m.jpeg",
    categories: ["all", "choice", "mobile"],
  },
  {
    name: "Idle Breakout",
    link: "./idlebreakout/",
    image: "./idlebreakout/idle.png",
    categories: ["all"],
  },
  {
    name: "Snow Battle",
    link: "./snowbattle/",
    image: "./snowbattle/img/logo.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Monkey Mart",
    link: "./monkeymart/",
    image: "./monkeymart/monkeymart.webp",
    categories: ["all", "choice", "mobile"],
  },
  {
    name: "Asteroids",
    link: "./astroids/",
    image: "./astroids/asteroids.jpg",
    categories: ["all"],
  },
  {
    name: "Coil",
    link: "./coil/",
    image: "./coil/favicon.ico",
    categories: ["all", "mobile"],
  },
  {
    name: "Digger",
    link: "./digger/",
    image: "./digger/digger.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Cluster Rush",
    link: "./cluster/",
    image: "./cluster/cluster.png",
    categories: ["all"],
  },
  {
    name: "Mario 5",
    link: "./mario5/",
    image: "./mario5/mario5.jpeg",
    categories: ["all"],
  },
  {
    name: "Duck Hunt",
    link: "./duckhunt/",
    image: "./duckhunt/duckhunt.jpeg",
    categories: ["all"],
  },
  {
    name: "Pong",
    link: "./pong/",
    image: "./pong/pong.jpg",
    categories: ["all", "2p"],
  },
  {
    name: "Pacman",
    link: "./pacman/",
    image: "./pacman/pacman.png",
    categories: ["all"],
  },
  {
    name: "Donkey Kong",
    link: "./donkeykong/",
    image: "./donkeykong/donkeykong.jpeg",
    categories: ["all"],
  },
  {
    name: "Sinuous",
    link: "./sinuous/",
    image: "./sinuous/sinuous.jpeg",
    categories: ["all"],
  },
  {
    name: "Missiles",
    link: "./missles/",
    image: "./missles/missles.png",
    categories: ["all", "mobile"],
  },
  {
    name: "LSD",
    link: "./lsd/",
    image: "./lsd/images/lsd_logo_tiny.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Radius Raid",
    link: "./radiusraid/",
    image: "./radiusraid/radiusraid.png",
    categories: ["all"],
  },
  {
    name: "Evil Glitch",
    link: "./evilglitch/",
    image: "./evilglitch/evilglitch.jpg",
    categories: ["all"],
  },
  {
    name: "Forests",
    link: "./forests/",
    image: "./forests/forests.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Frogger",
    link: "./frogger/",
    image: "./frogger/frogger.jpg",
    categories: ["all"],
  },
  {
    name: "Stack",
    link: "./stack/",
    image: "./stack/stack.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Pond",
    link: "./pond/",
    image: "./pond/pond.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Underrun",
    link: "./underrun/",
    image: "./underrun/underrun.png",
    categories: ["all"],
  },
  {
    name: "Cubefield",
    link: "./cubefield/",
    image: "./cubefield/cubefield.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Back Country",
    link: "./backcountry/",
    image: "./backcountry/backcountry.png",
    categories: ["all", "mobile"],
  },
  {
    name: "Papery Planes",
    link: "./papery-planes/",
    image: "./papery-planes/splash.jpg",
    categories: ["all"],
  },
  {
    name: "Sort the Court",
    link: "./sort-the-court/",
    image: "./sort-the-court/img/splash.png",
    categories: ["all", "choice"],
  },
];

function render(cat) {
  document.getElementById("games").innerHTML = "";
  if (cat == "all") {
    for (var i = 0; i < applist.length; i++) {
      var app = applist[i];
      var a = document.createElement("a");
      a.href = app.link;
      var img = document.createElement("img");
      img.src = app.image;
      img.classList.add("gameappframe");
      img.alt = app.name;
      //var name = document.createElement("h2");
      //name.innerHTML = app.name;
      a.appendChild(img);
      //  a.appendChild(name);

      if (
        app.categories.includes("beta") &&
        localStorage.getItem("beta") !== "enrolled"
      ) {
        a.style.display = "none";
      }
      document.getElementById("games").appendChild(a);
      // Add animation CSS class
a.classList.add('float-in'); 




      a.addEventListener("click", function () {
        //sessionStorage.setItem("encodedUrl", e.href);
      });
    }
  } else {
    for (var i = 0; i < applist.length; i++) {
      var app = applist[i];
      if (app.categories.includes(cat)) {
        var a = document.createElement("a");
        a.href = app.link;
        var img = document.createElement("img");
        img.src = app.image;
        img.classList.add("gameappframe");
        img.alt = app.name;

        a.appendChild(img);
        a.classList.add('float-in');
        if (
          app.categories.includes("beta") &&
          localStorage.getItem("beta") !== "enrolled"
        ) {
          a.style.display = "none";
        }
        if (app.categories.includes(cat)) {
          document.getElementById("games").appendChild(a);
          a.addEventListener("click", function () {
            sessionStorage.setItem("encodedUrl", app.link);
          });
        }
      }
    }
  }
}

const cat = document.getElementById("cat");
cat.addEventListener("change", function () {
  var scat = cat.value;
  render(scat);
});

const navi = navigator;
if(navi.userAgentData =! undefined) {
  const isMobile = navigator.userAgentData.mobile; 
  if(isMobile !== true) {
    render("all");
  } else {
    cat.value = "mobile";
    render("mobile");
  }
}
